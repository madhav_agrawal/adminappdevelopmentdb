package admin.lokacart.ict.mobile.com.adminapp;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by SidRama on 19/01/16.
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver {

    private String TAG = "NotificationBroadcastReceiver";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        int code = intent.getIntExtra("code",0);
        int id = intent.getIntExtra("id",0);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String org = sharedPrefs.getString("abbr","null");
      //  context_app = context;
        if (code == 0)
        {
          //  Toast.makeText(context, "Member approved", Toast.LENGTH_SHORT).show();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put("orgabbr",org);
                jsonObject.put("userId",id);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            AsyncT task = new AsyncT(context);
            task.execute(context,jsonObject,code);
        }
        else if (code == 1)
        {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
        }

        else if (code == 2) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
            String s = sharedPrefs.getString("memberNotifs",null);
            Type listOfTestObject = new TypeToken<List<NotificationBundle>>() {
            }.getType();
            Gson gson = new Gson();
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            List<NotificationBundle> notifsBundle;
            notifsBundle = gson.fromJson(s, listOfTestObject);
            Iterator<NotificationBundle> iter = notifsBundle.iterator();
            while(iter.hasNext())
            {
                jsonArray.put(iter.next().getId());
            }
            try
            {
                jsonObject.put("orgabbr",org);
                jsonObject.put("userIds",jsonArray);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            AsyncT task = new AsyncT(context);
            task.execute(context,jsonObject,code);
        }

    }
    class AsyncT extends AsyncTask<Object, Void, String>
    {
        Context context;
        AsyncT(Context context)
        {
            this.context = context;
        }
        protected String doInBackground(Object ... params)
        {
            GetJSON getJson = new GetJSON();
            Context context_app = (Context)params[0];
           // int code = (int)params[1];
            JSONObject jsonObject = (JSONObject)params[1];
            int code = (int)params[2];
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context_app);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.remove("memberNotifs");
            editor.apply();
            String emailid = sharedPrefs.getString("emailid","abc@gmail.com");
            String password = sharedPrefs.getString("password","null");
            String response = "null";
            if (code == 0 )
            {
                response = getJson.getJSONFromUrl(Master.serverURL+"/app/approve", jsonObject, "POST", false, null, null);
            }
            else if ( code == 2)
            {
                response = getJson.getJSONFromUrl(Master.serverURL+"/app/approveAll", jsonObject, "POST", false, null, null);
            }
                JSONObject obj = null;
            try
            {
                obj = new JSONObject(response);
            }
            catch(JSONException e)
            {
                    e.printStackTrace();
            }

            String data = "null";
            try
            {
                data =  obj.getString("response");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return data;
        }
        protected void onPostExecute(String response)
        {
            Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
        }
    }

}
