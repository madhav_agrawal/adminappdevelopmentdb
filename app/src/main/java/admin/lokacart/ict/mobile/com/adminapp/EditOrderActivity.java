package admin.lokacart.ict.mobile.com.adminapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import admin.lokacart.ict.mobile.com.adminapp.containers.Product;

/**
 * Created by madhav on 1/7/16.
 */
public class EditOrderActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    String[] itemList;
    SavedOrder savedOrder;
    Button bSaveChanges;
    int position,quantity;
    EditPlacedOrderAdapter editPlacedOrderAdapter;
    protected LayoutManagerType mCurrentLayoutManagerType;
    int orderid;
    String minimumBillAmount;


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    LinearLayoutManager layoutManager;

    TextView tOrders;
    String stockEnabledStatus;
    TextView tCartTotal;
    private String TAG = "EditOrderActivity";

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.title_edit_order));
        setContentView(R.layout.activity_edit_order);
        savedOrder = (SavedOrder) getIntent().getSerializableExtra("selectedOrder");
        position = getIntent().getIntExtra("position",-1);
        stockEnabledStatus = getIntent().getStringExtra("stockEnabledStatus");
        minimumBillAmount=getIntent().getStringExtra("minimumBillAmount");
        //System.out.println(minimumBillAmount.toString());
        ((LokacartAdminApplication) getApplication()).AnalyticsActivity(TAG);

        bSaveChanges=(Button) findViewById(R.id.bSaveChanges);
        bSaveChanges.setOnClickListener(this);

        tCartTotal=(TextView) findViewById(R.id.tCartTotal);

        Master.editOrderList = new ArrayList<Product>();
        Master.changeCheckList= new ArrayList<>();

        for(int i=0; i < savedOrder.getPlacedOrderItemsList(position).size(); i++) {

            itemList=savedOrder.getPlacedOrderItemsList(position).get(i);

            //double total = quantity * Double.parseDouble(entry.getValue());
            //  Total += Double.parseDouble(String.format("%.2f", total));
            Product item = new Product(itemList[0], Integer.parseInt(itemList[2])
                    , Integer.parseInt(itemList[4]), Double.parseDouble(itemList[1])
                    , Double.parseDouble(String.format("%.2f",Double.parseDouble(itemList[3]))));

            Master.changeCheckList.add(Integer.parseInt(itemList[2]));
            Master.editOrderList.add(item);
            //System.out.println(Master.editOrderList.get(i).getName());
        }

        layoutManager = new LinearLayoutManager(this);


        mRecyclerView = (RecyclerView) findViewById(R.id.rvCart);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);


       /* mLayoutManager = new LinearLayoutManager(this);
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);*/

        editPlacedOrderAdapter = new EditPlacedOrderAdapter(this,tCartTotal,stockEnabledStatus);
        mRecyclerView.setAdapter(editPlacedOrderAdapter);
        editPlacedOrderAdapter.notifyDataSetChanged();


      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

    }

    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType)
        {

            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(this);
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        getSupportFragmentManager().popBackStack();
        return true;
    }


    @Override
    public void onClick(View v) {


        double setTotal=0.0;
        double minBillAmt = Double.parseDouble(minimumBillAmount);

        for(int i=0;i<Master.editOrderList.size();i++)
        {
            setTotal=setTotal+Master.editOrderList.get(i).getTotal();
            // System.out.println("updating---------------------"+sum);
        }

        if(setTotal<minBillAmt){
            Master.alertDialog(EditOrderActivity.this,getString(R.string.dialog_alert_total_less_than_min_bill_amt),getString(R.string.label_alertdialog_ok));

        }
        else if(checkChange() && setTotal>=minBillAmt)
        {
            if(Master.isNetworkAvailable(this))
            {
                try {
                   // boolean qty=false;
                    JSONObject order = new JSONObject();
                    order.put("orgabbr",AdminDetails.getAbbr());
                    order.put("groupname", "Parent Group");
                    order.put("status","saved");
                    order.put("comments","null");
                    JSONArray products = new JSONArray();
                    JSONObject object;

                    for(int i=0;i<Master.editOrderList.size();i++)
                    {
                        object  = new JSONObject();
//                            System.out.println("edit product name ------------"+Master.editOrderList.get(i).getName()+ " quantity----------"+Master.editOrderList.get(i).getQuantity());
                        object.put("name",Master.editOrderList.get(i).getName());

                        /*double itemTotal = Master.editOrderList.get(i).getTotal();
                        if(itemTotal==0.0)
                        {
                            qty=true;
                            break;
                        }*/
                        object.put("quantity",Master.editOrderList.get(i).getQuantity());
                        products.put(object);
                    }
                    order.put("orderItems",products);

                    //System.out.println(order.toString());

                        System.out.println("editing placed order json------------"+order.toString());

                    orderid=getIntent().getExtras().getInt("orderId");

                    new UpdateOrderTask(this,orderid).execute(order);


                  /*  if(!qty){
                        new UpdateOrderTask(this,orderid).execute(order);
                         }
                    else
                        Master.alertDialog(this, getString(R.string.dialog_alert_please_insert_quantity), getString(R.string.label_alertdialog_ok));
*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //}
            }
            else
            {
                Master.alertDialog(this, getString(R.string.label_toast_no_network_connection), getString(R.string.label_alertdialog_ok));
            }
        }
        else
        {
            Toast.makeText(this, R.string.label_toast_no_changes_to_save, Toast.LENGTH_LONG).show();
        }

    }

    boolean checkChange()
    {

        for(int i=0;i<Master.editOrderList.size();i++)
        {

            int quantity = Master.editOrderList.get(i).getQuantity();
            if(Master.changeCheckList.get(i) != quantity)
                return true;
        }

        return false;
    }
    //---------------------------------------------------API for Updating the changes------------------------------------

    private class UpdateOrderTask extends AsyncTask<JSONObject,String,String>
    {
        ProgressDialog pd;
        int id;
        String response,val;
        public UpdateOrderTask(Context context, int id){
            this.id=id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(EditOrderActivity.this);
            pd.setMessage(getString(R.string.pd_updating_order));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
            //  String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+id;
            response = getJson.getJSONFromUrl(Master.getEditPlacedOrderURL(id),params[0],"POST",true,AdminDetails.getEmail(),AdminDetails.getPassword());
            // response = getJson.getJSONFromUrl(Master.getEditPlacedOrderURL(id),params[0],"POST",true,MemberDetails.getEmail(),MemberDetails.getPassword());
//            System.out.println("--------update detail response--------"+response);
            return response;
        }

        @Override
        protected void onPostExecute(String s) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(s.equals("exception"))
            {
                Master.alertDialog(EditOrderActivity.this, getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
            }
            else {
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(s);
                    val=jsonObj.getString("status");

                    if(val.equals("Success]"))
                    {

                        Toast.makeText(EditOrderActivity.this,R.string.label_toast_order_updated_succesfully, Toast.LENGTH_SHORT).show();

                         //System.out.println("updated order-----------" + jsonObj.getJSONObject("order"));


                        SavedOrder order = new SavedOrder(jsonObj.getJSONObject("order"),position,Master.PLACEDORDER);

                        Master.savedOrderArrayList.set(position, order);
                        //PlacedOrderFragment.orderAdapter.notifyDataSetChanged();


                       // PlacedOrderFragment placedOrderFragment = new PlacedOrderFragment();
                        //placedOrderFragment.clickListener(position);
                        finish();
                        getSupportFragmentManager().popBackStack();
                    }
                    else  if(val.equals("Failure"))
                    {
                        String error[] = {jsonObj.getString("error")};
                        Toast.makeText(EditOrderActivity.this, error[0], Toast.LENGTH_LONG).show();
                    }
                    else if(val.equals("failure") && jsonObj.getString("error").equals("Order has already been deleted")){
                        Toast.makeText(EditOrderActivity.this,getString(R.string.label_toast_save_changes_to_deleted_order),Toast.LENGTH_LONG).show();

                    }
                    else if(val.equals("Order cannot be modified. The order has been deleted by the User")){
                        Toast.makeText(EditOrderActivity.this,getString(R.string.label_toast_save_changes_to_deleted_order),Toast.LENGTH_LONG).show();

                    }
                    else {
                        Toast.makeText(EditOrderActivity.this, R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                    }
                    } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}
