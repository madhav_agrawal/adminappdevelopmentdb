package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.MyListener;
import admin.lokacart.ict.mobile.com.adminapp.ProductActivity;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.containers.Product;
import admin.lokacart.ict.mobile.com.adminapp.containers.ProductType;

/**
 * Created by Vishesh on 22-02-2016.
 */


public class ProductTypeFragment extends Fragment{
    View productFragmentView;
    public  RecyclerView mRecyclerView;
    public  RecyclerViewAdapter mAdapter;
    public  RecyclerView.LayoutManager mLayoutManager;
    Menu searchMenu;
    MenuItem search,up,down,edit,delete,enable_disable;
    private SwipeRefreshLayout swipeContainer;

    public ArrayList<String> getProductTypeArrayList() {
        return productTypeArrayList;
    }

    public void setProductTypeArrayList(ArrayList<String> productTypeArrayList) {
        this.productTypeArrayList = productTypeArrayList;
    }

    public ArrayList<String> productTypeArrayList;

    int recyclerViewIndex;
    Dialog dialog;
    JSONObject responseObject;
    ProgressDialog progressDialog;
    String newProductTypeName;
    public final String LOG_TAG = "ProductTypeFrag";
    FloatingActionButton fab;
    RecyclerViewAdapter.DataObjectHolder dataObjectHolder;
    AlertDialog.Builder builder;
    ProductType productType;
    Product product;
    int count = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);
        getActivity().setTitle(R.string.title_product_types);
        productTypeArrayList = new ArrayList<String>();
        recyclerViewIndex = 0;

        setHasOptionsMenu(true);

        if(getActivity()!=null && ProductTypeFragment.this.isAdded())
            ((DashboardActivity)getActivity()).updateStatusBarColor();

        new GetAdminAndProductTypeDetails(savedInstanceState).execute();

        fab= (FloatingActionButton) getActivity().findViewById(R.id.fab);

        return productFragmentView;

    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = (RecyclerView) productFragmentView.findViewById(R.id.productTypeRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        swipeContainer = (SwipeRefreshLayout) productFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetAdminAndProductTypeDetails(savedInstanceState).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //new GetAdminAndProductTypeDetails(getActivity()).execute();


    }

    public void DisplayData(){
      //  mAdapter = new RecyclerViewAdapter(getProductTypeArrayList(), getActivity(),this);
        mAdapter = new RecyclerViewAdapter(getActivity(),this);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void clickListener(int position) {
        Intent productIntent = new Intent(getActivity(), ProductActivity.class);
      //  productIntent.putExtra("product type", (String) productTypeArrayList.get(position));
          productIntent.putExtra("product type", Master.productTypeDisplayList.get(position).getName());
        productIntent.putExtra("position", position);


        startActivity(productIntent);
    }

/*    public void longClickListener(final int position) {

        final EditText eEditProductTypeName, eEditProductTypeUnit;
        final Button bEditProductTypeConfirm, bEditProductTypeCancel, bEditProductTypeDelete, bEditProductTypeEdit;

        final String selectedProductType = (String) productTypeArrayList.get(position);

        if (Master.isNetworkAvailable(getActivity())) {

            dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.product_type_box);
            dialog.setTitle("Modify " + selectedProductType);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            eEditProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
            eEditProductTypeName.setVisibility(View.GONE);

            eEditProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
            eEditProductTypeUnit.setVisibility(View.GONE);

            bEditProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
            bEditProductTypeConfirm.setVisibility(View.GONE);

            bEditProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

            bEditProductTypeCancel.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeDelete.setOnClickListener(new View.OnClickListener() {
                                                          @Override
                                                          public void onClick(View v) {
                                                              JSONObject jsonObject = new JSONObject();
                                                              try {
                                                                  jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                                                  jsonObject.put("name", selectedProductType);
                                                              } catch (JSONException e) {
                                                              }
                                                              new DeleteProductTypeTask(position).execute(jsonObject);
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeEdit.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            bEditProductTypeDelete.setVisibility(View.GONE);
                                                            bEditProductTypeEdit.setVisibility(View.GONE);

                                                            eEditProductTypeName.setVisibility(View.VISIBLE);
                                                            eEditProductTypeUnit.setVisibility(View.VISIBLE);
                                                            bEditProductTypeConfirm.setVisibility(View.VISIBLE);

                                                            int indexOfBracket = selectedProductType.indexOf('(');
                                                            String pName, pUnit;
                                                            pName = selectedProductType.substring(0, indexOfBracket - 1);
                                                            pUnit = selectedProductType.substring(indexOfBracket + 2, selectedProductType.length() - 2);
                                                            eEditProductTypeName.setText(pName);
                                                            eEditProductTypeUnit.setText(pUnit);
                                                        }
                                                    }
            );

            bEditProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (eEditProductTypeName.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                    } else if (eEditProductTypeUnit.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                    } else {
                        String updatedProductType = eEditProductTypeName.getText().toString().trim() + " ( " + eEditProductTypeUnit.getText().toString().trim() + " )";
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("oldname", selectedProductType);
                            jsonObject.put("newname", updatedProductType);
                        } catch (JSONException e) {
                        }
                        new UpdateProductTypeTask(updatedProductType, position).execute(jsonObject);
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        } else {
            Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }



    }*/

    public void addNewProductType() {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.product_type_box);
        dialog.setTitle(R.string.dialog_title_add_product_type);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductTypeName, eProductTypeUnit;
        final Button bProductTypeConfirm, bProductTypeCancel, bProductTypeDelete, bProductTypeEdit;
        final TextView tProductTypeChars;

        eProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
        eProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
        tProductTypeChars = (TextView) dialog.findViewById(R.id.tProductTypeChars);

        eProductTypeName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    tProductTypeChars.setText((15 - s.length()) + " " +  getString(R.string.textview_characters_left));
            }
        });

        bProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
        bProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

        bProductTypeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO send the new product name
                if(eProductTypeUnit.getText().toString().trim().equals("")){
                    newProductTypeName = eProductTypeName.getText().toString().trim();
                }else
                newProductTypeName = eProductTypeName.getText().toString().trim() + " ( " +
                        eProductTypeUnit.getText().toString().trim() + " )";

                if (eProductTypeName.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                } /*else if (eProductTypeUnit.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                }*/ else if (!Master.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                } else {
                    boolean flag = true;

                    for (int i = 0; i < recyclerViewIndex; ++i) {
                        if (productTypeArrayList.get(i).equals(newProductTypeName)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", newProductTypeName);
                            new AddProductTypeTask(getActivity(), newProductTypeName).execute(jsonObject);
                        } catch (JSONException e) {

                        }

                    }
                    else {
                        Toast.makeText(getActivity(), R.string.label_toast_please_enter_unique_product_type, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void saveChanges(){
        Master.fabClickKey=0;
        fab.setImageResource(R.drawable.ic_add_white_48dp);
        //here call the save API
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject;
        for(int i=0;i<mAdapter.getItemCount();i++){
            try {
                jsonObject=new JSONObject();
                jsonObject.put("productType",productTypeArrayList.get(i));
                jsonObject.put("seq",i+"");
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

       JSONObject jsonObjectParent = new JSONObject();
        try {
            jsonObjectParent.put("orgabbr", AdminDetails.getAbbr());
            jsonObjectParent.put("sequence", jsonArray);
        } catch (JSONException e) {
        }
        new SaveProductList(getActivity()).execute(jsonObjectParent);


       // editInvisible(false);
        //productTypeArrayList.clear();
        //new GetAdminAndProductTypeDetails(getActivity()).execute();

    }



    @Override
    public void onResume() {
        super.onResume();

        getActivity().invalidateOptionsMenu();

      //  Log.e("ProdTye frag", "in onResume");

        //getActivity().setTitle("Product Types");
        if(mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }

        fab.setImageResource(R.drawable.ic_add_white_48dp);
        Master.fabClickKey=0;
        Master.backPress=0;
        getActivity().onBackPressed();

    }

    public void editInvisible(boolean value) {

        if (!value) {
            UnClickChangeColor();
            delete.setVisible(false);
            up.setVisible(false);
            down.setVisible(false);
            edit.setVisible(false);
            enable_disable.setVisible(false);
            search.setVisible(true);

        } else {
            OnClickChangeColor();
            delete.setVisible(true);
            up.setVisible(true);
            down.setVisible(true);
            edit.setVisible(true);
            enable_disable.setVisible(true);
            search.setVisible(false);
            if(Master.productTypeDisplayList.get(mAdapter.getCurrentPosition()).productTypeStatus == 0){
                enable_disable.setIcon(R.drawable.product_type_enable);
            }
            else{
                enable_disable.setIcon(R.drawable.product_type_disable);
            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

       // Log.e("ProdTye frag", "in onCreateOptionsMenu");
        search = menu.findItem(R.id.search);
        delete = menu.findItem(R.id.bProductTypeDelete);
        edit = menu.findItem(R.id.bProductTypeEdit);
        up = menu.findItem(R.id.bProductTypeUpward);
        down = menu.findItem(R.id.bProductTypeDonward);
        enable_disable = menu.findItem(R.id.bProductTypeEnableDisable);

        // editInvisible(false);
        delete.setVisible(false);
        up.setVisible(false);
        down.setVisible(false);
        edit.setVisible(false);
//        search.setVisible(true);
        enable_disable.setVisible(false);
        search.setVisible(true);

        if(getActivity()!=null && ProductTypeFragment.this.isAdded())
            ((DashboardActivity)getActivity()).updateStatusBarColor();

    }

    public void ProductTypeFunctions(String function){

       // final String selectedProductType = productTypeArrayList.get(mAdapter.getCurrentPosition());
         final String selectedProductType = Master.productTypeDisplayList.get(mAdapter.getCurrentPosition()).getName();

        switch (function) {

            case "Up":

                Master.fabClickKey = 1;
                fab.setImageResource(R.drawable.ic_done_all_black_24dp);
                fab.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                fab.setEnabled(true);
                if (mAdapter.getCurrentPosition() != 0 && mAdapter.getCurrentPosition() < mAdapter.getItemCount()) {
                    mAdapter.onItemMove(mAdapter.getCurrentPosition(), mAdapter.getCurrentPosition() - 1);
                    mAdapter.setCurrentPosition(mAdapter.getCurrentPosition() - 1);
                }
             break;

            case "Down":
                    Master.fabClickKey = 1;
                    fab.setImageResource(R.drawable.ic_done_all_black_24dp);
                    fab.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    fab.setEnabled(true);

                    if (mAdapter.getCurrentPosition() < mAdapter.getItemCount() - 1) {
                        mAdapter.onItemMove(mAdapter.getCurrentPosition(), mAdapter.getCurrentPosition() + 1);
                        mAdapter.setCurrentPosition(mAdapter.getCurrentPosition() + 1);
                    }
                break;

            case "Delete" :
                    builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getString(R.string.builder_title_delete) + " " + selectedProductType + " ?");
                    builder.setPositiveButton(R.string.builder_confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                jsonObject.put("name", selectedProductType);
                                new DeleteProductTypeTask(mAdapter.getCurrentPosition()).execute(jsonObject);

                            } catch (JSONException e) {
                            }

                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton(R.string.builder_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });

                    builder.create();
                    builder.show();
                break;

            case "EnableDisable":
                Master.fabClickKey = 1;
                fab.setImageResource(R.drawable.ic_done_all_black_24dp);
                fab.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                fab.setEnabled(true);
                builder = new AlertDialog.Builder(getActivity());

                if(Master.productTypeDisplayList.get(mAdapter.getCurrentPosition()).productTypeStatus==0)
                {
                    builder.setTitle(getString(R.string.builder_title_enable) +" "+ selectedProductType + " ?");
                    builder.setPositiveButton(R.string.builder_confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("id", Master.productTypeDisplayList.get(mAdapter.getCurrentPosition()).productTypeId);
                                jsonObject.put("status",1);
                                new EnableDisableProductTypeTask(mAdapter.getCurrentPosition()).execute(jsonObject);

                            } catch (JSONException e) {
                            }

                            dialog.dismiss();
                        }
                    });

                }
                else
                {
                    builder.setTitle(getString(R.string.builder_title_disable) +" "+ selectedProductType + " ?");
                    builder.setPositiveButton(R.string.builder_confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("id", Master.productTypeDisplayList.get(mAdapter.getCurrentPosition()).productTypeId);
                                jsonObject.put("status",0);
                                new EnableDisableProductTypeTask(mAdapter.getCurrentPosition()).execute(jsonObject);

                            } catch (JSONException e) {
                            }

                            dialog.dismiss();
                        }
                    });

                }

                builder.setNegativeButton(R.string.builder_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });

                builder.create();
                builder.show();


               /* if (mAdapter.getCurrentPosition() < mAdapter.getItemCount() - 1) {
                    mAdapter.onItemMove(mAdapter.getCurrentPosition(), mAdapter.getCurrentPosition() + 1);
                    mAdapter.setCurrentPosition(mAdapter.getCurrentPosition() + 1);
                }*/
                break;

            case "Edit":

                final EditText eEditProductTypeName, eEditProductTypeUnit;
                final Button bEditProductTypeConfirm, bEditProductTypeCancel;
                final TextView tProductTypeChars;

                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.product_type_box);
                dialog.setTitle(getString(R.string.dialog_title_modify) + " " +selectedProductType);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);

                eEditProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
                eEditProductTypeName.setVisibility(View.VISIBLE);

                eEditProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
                eEditProductTypeUnit.setVisibility(View.VISIBLE);

                tProductTypeChars = (TextView) dialog.findViewById(R.id.tProductTypeChars);
                tProductTypeChars.setText((15 - eEditProductTypeName.toString().length()) + " " +  getString(R.string.textview_characters_left));

                eEditProductTypeName.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {}

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                        if(s.length() != -1)
                            tProductTypeChars.setText((15 - s.length()) + " " +  getString(R.string.textview_characters_left));
                    }
                });

                int indexOfBracket = selectedProductType.indexOf('(');
                final String pName, pUnit;

                if(indexOfBracket!=-1){
                    pName = selectedProductType.substring(0, indexOfBracket - 1);
                    pUnit = selectedProductType.substring(indexOfBracket + 2, selectedProductType.length() - 2);
                    eEditProductTypeName.setText(pName);
                    eEditProductTypeUnit.setText(pUnit);}
                else{
                    pUnit="";
                    eEditProductTypeUnit.setText(pUnit);
                    pName=selectedProductType.toString();
                    eEditProductTypeName.setText(pName);
                }

                bEditProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
                bEditProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

                bEditProductTypeCancel.setOnClickListener(new View.OnClickListener() {
                                                              @Override
                                                              public void onClick(View v) {
                                                                  dialog.dismiss();
                                                              }
                                                          }
                );

                bEditProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (eEditProductTypeName.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                        }
                        else if(eEditProductTypeName.getText().toString().trim().equals(pName) && eEditProductTypeUnit.getText().toString().trim().equals(pUnit)){
                            Toast.makeText(getActivity(), R.string.label_toast_no_changes_made, Toast.LENGTH_SHORT).show();
                            if(Master.fabClickKey==0) {
                               // mAdapter = new RecyclerViewAdapter(productTypeArrayList, getActivity(), ProductTypeFragment.this);
                                 mAdapter = new RecyclerViewAdapter(getActivity(), ProductTypeFragment.this);


                                mRecyclerView.setAdapter(mAdapter);
                                editInvisible(false);
                            }

                            dialog.dismiss();
                        }
                        else {
                            String updatedProductType;
                            if(eEditProductTypeUnit.getText().toString().trim().equals("")){
                                updatedProductType = eEditProductTypeName.getText().toString().trim() + eEditProductTypeUnit.getText().toString().trim();
                            }
                            else
                                updatedProductType = eEditProductTypeName.getText().toString().trim() + " ( " + eEditProductTypeUnit.getText().toString().trim() + " )";

                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                jsonObject.put("oldname", selectedProductType);
                                jsonObject.put("newname", updatedProductType);
                            } catch (JSONException e) {
                            }
                            new UpdateProductTypeTask(updatedProductType, mAdapter.getCurrentPosition()).execute(jsonObject);
                            dialog.dismiss();
                        }
                    }
                });
                dialog.show();


                break;

            default:
                break;

        }

    }

    private void OnClickChangeColor() {
        // Initial colors of each system bar.
        final int statusBarColor = getResources().getColor(R.color.colorPrimary);
        final int toolbarColor = getResources().getColor(R.color.colorPrimaryDark);

        // Desired final colors of each bar.
        final int statusBarToColor = getResources().getColor(R.color.toColorPrimary);
        final int toolbarToColor = getResources().getColor(R.color.toColorPrimaryDark);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getActivity() != null) {
                    getActivity().getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getActivity() != null){
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(background);
                }
            }
        });

        anim.setDuration(150).start();
    }

    private void UnClickChangeColor() {
        // Initial colors of each system bar.
        final int statusBarColor = getResources().getColor(R.color.toColorPrimary);
        final int toolbarColor = getResources().getColor(R.color.toColorPrimaryDark);

        // Desired final colors of each bar.
        final int statusBarToColor = getResources().getColor(R.color.colorPrimary);
        final int toolbarToColor = getResources().getColor(R.color.colorPrimaryDark);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getActivity() != null) {
                    getActivity().getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getActivity() != null){
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(background);
                }
            }
        });

        anim.setDuration(150).start();
    }



    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }


//---------------------------------------------------------------------------------------------------------------------------//
    public class GetAdminAndProductTypeDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response;
        Bundle savedInstance;

        public GetAdminAndProductTypeDetails(Bundle savedInstance) {
            this.savedInstance = savedInstance;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String adminDetailsAndProductTypeURL = Master.getAdminDetailsAndProductTypeURL() + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(adminDetailsAndProductTypeURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
           //System.out.println("-------product type id-----------"+response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {

            if(pd != null && pd.isShowing())
              pd.dismiss();

            if(ProductTypeFragment.this.isAdded()){

                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server),getString(R.string.label_alertdialog_ok));
                } else {
                    try
                    {

                        responseObject = new JSONObject(response);
                        JSONArray jsonArray = responseObject.getJSONArray("products");
                        Master.productTypeSearchList =new ArrayList<>();
                        Master.productTypeDisplayList=new ArrayList<>();
                        recyclerViewIndex=0;

                        if(jsonArray.length() > 0)
                        {

                            for(int x=0; x < jsonArray.length(); x++)
                            {

                                JSONObject productTypeObject=jsonArray.getJSONObject(x);
                                Iterator<String> keysItr = productTypeObject.keys();

                                productType = new ProductType();

                                while (keysItr.hasNext())
                                {

                                    String key = keysItr.next();

                                    //System.out.println("key :"+key);
                                    //productTypeArrayList.add(x,key);

                                  //  productType=new ProductType(key);
                                    Object category = productTypeObject.get(key);

                                    if(category instanceof JSONArray)
                                    {

                                        productTypeArrayList.add(x,key);

                                        productType.name = key;

                                        for (int i = 0; i < ((JSONArray) category).length(); i++) {
                                            if (((JSONArray) category).getJSONObject(i).has("imageUrl") && ((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null && ((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")), "null", "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                } else if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                } else if (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0, Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                }
                                            } else if (((JSONArray) category).getJSONObject(i).has("imageUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            "null", "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                }
                                            } else if (((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")), "null", "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"));
                                                }
                                            } else {
                                                product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                        "null", "null", ((JSONArray) category).getJSONObject(i).getString("description"),
                                                        ((JSONArray) category).getJSONObject(i).getString("id"));
                                            }

                                            productType.productItems.add(product);
                                        }

                                        if (productType.productItems.size() > 0) {
                                            Master.productTypeSearchList.add(productType);

                                        }

                                    }

                                    if(key.equals("productId"))
                                    {
                                        //System.out.println("category---------" + category);
                                        productType.productTypeId=((Integer)category);
                                    }
                                    if(key.equals("status"))
                                    {
                                        //System.out.println("category---------" + category);
                                        productType.productTypeStatus =((Integer)category);
                                    }

                                }

                                Master.productTypeDisplayList.add(productType);

                            }

                          /*  for(int i=0; i < Master.productTypeSearchList.size();i++){

                                for(int j=0; j < Master.productTypeSearchList.get(i).productItems.size();j++)

                                System.out.println(Master.productTypeSearchList.get(i).productItems.get(j).getName());
                            }*/

                            DashboardActivity.updateSearchAdapter();
                        }


                       /* for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                        {
                            productTypeArrayList.add(recyclerViewIndex, jsonArray.getString(recyclerViewIndex));
                        }*/
                       // setProductTypeArrayList(productTypeArrayList);
                        ++count;
                        if(count<2) {

                            DisplayData();
                            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.productTypeClickKey, ProductTypeFragment.this));
                            mAdapter.notifyDataSetChanged();
                        }
                        else {
                            mRecyclerView.swapAdapter(mAdapter, true);
                        }
                        swipeContainer.setRefreshing(false);
                    }
                    catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }

        }
    }

    //-----------------------Class for updating a product type-------------------------------------
    class UpdateProductTypeTask extends AsyncTask<JSONObject, String, String> {
        String response, editProductTypeName;
        int editProductTypePosition;

        UpdateProductTypeTask(String editProductTypeName, int editProductTypePosition) {
            this.editProductTypeName = editProductTypeName;
            this.editProductTypePosition = editProductTypePosition;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getEditProductTypeURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if(ProductTypeFragment.this.isAdded()){
                try {
                    responseObject = new JSONObject(message);
                    message = responseObject.getString("edit");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (message.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else if (message.equals("success")) {
                    Toast.makeText(getActivity(), R.string.label_toast_Product_type_updated_successfully, Toast.LENGTH_SHORT).show();
                    productTypeArrayList.set(editProductTypePosition, editProductTypeName);
                    if(Master.fabClickKey==0) {
                        //mAdapter = new RecyclerViewAdapter(productTypeArrayList, getActivity(), ProductTypeFragment.this);
                        mAdapter = new RecyclerViewAdapter(getActivity(), ProductTypeFragment.this);

                        mRecyclerView.setAdapter(mAdapter);
                        editInvisible(false);
                    }
                    else
                        mAdapter.notifyItemChanged(editProductTypePosition);

                } else {
                    Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                }
            }

        }
    }
//---------------------------------------------------------------------------------------------

    //-----------------------Class for deleting a product type-------------------------------------

    /****************
     * API Format************************
     * API:
     * api/producttype/delete
     * <p/>
     * Body:
     * {
     * "orgabbr":"Test2",
     * "name":"aba (kg)"
     * }
     * Response:
     * {"message":"Product Type cannot be deleted as products of this type have been ordered"}
     * OR
     * {  "message":"Successfully deleted Product Type" }
     ***************************************************/
    class DeleteProductTypeTask extends AsyncTask<JSONObject, String, String> {
        String response;
        int productTypePosition;

        DeleteProductTypeTask(int productTypePosition) {
            this.productTypePosition = productTypePosition;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteProductTypeURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
           // System.out.println("Delete response: " + response);
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(progressDialog!= null && progressDialog.isShowing())
                progressDialog.dismiss();

            if(ProductTypeFragment.this.isAdded()){

                try {
                    responseObject = new JSONObject(message);
                    System.out.println(responseObject.toString());
                    message = responseObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!Master.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT);
                } else if (message.equals("exception")) {
                    Master.alertDialog(getActivity(),getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else if (message.equals("Successfully deleted Product Type")) {
                    Toast.makeText(getActivity(), R.string.label_toast_product_type_deleted_successfully, Toast.LENGTH_SHORT).show();

                 //   productTypeArrayList.remove(productTypePosition);
                    Master.productTypeDisplayList.remove(productTypePosition);
                    mAdapter.notifyItemRemoved(productTypePosition);
                    mAdapter.setFlag(false);
                    mAdapter.setCurrentPosition(mAdapter.getItemCount()+1);
                    editInvisible(false);
                    recyclerViewIndex--;

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------


    public class AddProductTypeTask extends AsyncTask<Object, String, String> {

        String response;
        ProgressDialog progressDialog;
        Context context;
        String newProductType;
        Master master;
        MyListener callback;

        AddProductTypeTask(Context context, String newProductType) {
            this.context = context;
            this.newProductType = newProductType;
            this.callback = (MyListener) context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Object... params) {
            master = new Master();
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(master.getAddNewProductTypeUrl(), (JSONObject) params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {

            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if(ProductTypeFragment.this.isAdded()){
                if (response.equals("exception")) {
                    master.alertDialog(getActivity(),getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else {
                    try {
                        responseObject = new JSONObject(response);
                    } catch (JSONException e) {
                    }
                    try {
                        response = responseObject.getString("upload");
                        if (response.equals("success")) {

                            int id = responseObject.getInt("id");
                            int status = responseObject.getInt("status");

                            Toast.makeText(getActivity(), R.string.label_toast_Product_type_added_successfully, Toast.LENGTH_SHORT).show();

                            ProductType ptype = new ProductType(newProductTypeName);
                            ptype.productTypeStatus=status;
                            ptype.productTypeId=id;
                            Master.productTypeDisplayList.add(recyclerViewIndex++,ptype);

                           // productTypeArrayList.add(recyclerViewIndex++, newProductTypeName);
                            mAdapter.notifyItemInserted(recyclerViewIndex - 1);
                            mAdapter.notifyDataSetChanged();
                            dialog.dismiss();

                        } else {

                            Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {

                    }
                }
            }


        }
    }

    public class SaveProductList extends AsyncTask<JSONObject, String, String>{

        String response;
        ProgressDialog progressDialog;
        Context context;
        Master master;


        SaveProductList(Context context){
        this.context=context;

        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            master= new Master();
            GetJSON getJSON = new GetJSON();
            response=getJSON.getJSONFromUrl(master.getSaveProductTypeURL(),(JSONObject) params[0],"POST",true,AdminDetails.getEmail(),AdminDetails.getPassword());
            return response;


        }

        @Override
        protected void onPostExecute(String message) {

            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if(ProductTypeFragment.this.isAdded()){
                try {
                    responseObject = new JSONObject(message);
                    System.out.println(responseObject.toString());
                    message = responseObject.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!Master.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT);
                } else if (message.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok) );
                } else if (message.equals("Success")) {
                    Toast.makeText(getActivity(), R.string.label_toast_Product_type_sequence_changed, Toast.LENGTH_SHORT).show();

                    // mAdapter.notifyItemMoved();
                    mAdapter.setFlag(false);
                    mAdapter.setCurrentPosition(mAdapter.getItemCount() + 1);
                    editInvisible(false);
                   // mAdapter = new RecyclerViewAdapter(productTypeArrayList, getActivity(), ProductTypeFragment.this);
                     mAdapter = new RecyclerViewAdapter(getActivity(), ProductTypeFragment.this);

                    mRecyclerView.setAdapter(mAdapter);
                    DisplayData();

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }

            }

        }
    }

    //---------------------------------------------------------------------------------------------

    //-----------------------Class for deleting a product type-------------------------------------

    /****************
     * API Format************************
     * API:
     * api/producttype/delete
     * <p/>
     * Body:
     * {
     * "orgabbr":"Test2",
     * "name":"aba (kg)"
     * }
     * Response:
     * {"message":"Product Type cannot be deleted as products of this type have been ordered"}
     * OR
     * {  "message":"Successfully deleted Product Type" }
     ***************************************************/
    class EnableDisableProductTypeTask extends AsyncTask<JSONObject, String, String> {
        String response;
        int productTypePosition;

        EnableDisableProductTypeTask(int productTypePosition) {
            this.productTypePosition = productTypePosition;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getEnableDisableProductTypeURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            //System.out.println("Delete response: " + AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {

            if(progressDialog!= null && progressDialog.isShowing())
                progressDialog.dismiss();

            if(ProductTypeFragment.this.isAdded()){

                try {
                    responseObject = new JSONObject(message);
                    System.out.println(responseObject.toString());
                    message = responseObject.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!Master.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT);
                } else if (message.equals("exception")) {
                    Master.alertDialog(getActivity(),getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else if (message.equals("Success")) {

                    //productTypeArrayList.remove(productTypePosition);
                    if(Master.productTypeDisplayList.get(productTypePosition).productTypeStatus==1)
                    {
                        Master.productTypeDisplayList.get(productTypePosition).productTypeStatus=0;
                        Toast.makeText(getActivity(), R.string.label_toast_product_type_disabled_successfully, Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        Master.productTypeDisplayList.get(productTypePosition).productTypeStatus=1;

                        Toast.makeText(getActivity(), R.string.label_toast_product_type_enabled_successfully, Toast.LENGTH_SHORT).show();

                    }
                    mAdapter.notifyItemChanged(productTypePosition);
                   // mAdapter.notifyItemRemoved(productTypePosition);
                    mAdapter.setFlag(false);
                    mAdapter.setCurrentPosition(mAdapter.getItemCount()+1);
                    editInvisible(false);
                    recyclerViewIndex--;

                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }



}
