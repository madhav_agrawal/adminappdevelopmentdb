package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import admin.lokacart.ict.mobile.com.adminapp.OrderDetailsRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Space;
import android.widget.TextView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

/**
 * Created by Vishesh on 19-01-2016.
 */
public class CancelledOrderFragment extends Fragment  {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private SwipeRefreshLayout swipeContainer;
    protected LayoutManagerType mCurrentLayoutManagerType;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    ArrayList<SavedOrder> cancelledOrderArrayList;
    View cancelledOrderFragmentView;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Master master;
    JSONObject responseObject;
    static int recyclerViewIndex;
    ArrayList<JSONObject> orderObjects;
    ArrayList<SavedOrder> orders;
    private int count =0;
    TextView tOrders;
    private int apiCalls=0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        cancelledOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);
        swipeContainer = (SwipeRefreshLayout) cancelledOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {;
                new GetCancelledOrderDetails(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_bright);

        return cancelledOrderFragmentView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cancelledOrderArrayList = new ArrayList<SavedOrder>();
    }

    public ArrayList<SavedOrder> getOrders() {

        orders = new ArrayList<SavedOrder>();

        int count=0;

        if(orderObjects.size()!=0)
        {
            for(JSONObject entry : orderObjects){
                SavedOrder order = new SavedOrder(entry,count,Master.CANCELLEDORDER);
                orders.add(order);
                count++;
            }
        }

        else
        {

        }
        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

            new GetCancelledOrderDetails(true).execute();

        mRecyclerView = (RecyclerView) cancelledOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        tOrders = (TextView) cancelledOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_cancelled_orders_present);
        tOrders.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }
    public void clickListener(int position)
    {

        RecyclerView mRecyclerView;
        final RecyclerView.Adapter mAdapter;
        RecyclerView.LayoutManager mLayoutManager;

        Button processOrder,close,edit,delete;
        Space space,sProcessClose;
        TextView txtComments;

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.orders_details_list))
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)
                .setCancelable(true)
                .create();


        mRecyclerView = (RecyclerView)dialog.findViewById(R.id.orderdetailsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new OrderDetailsRecyclerViewAdapter(cancelledOrderArrayList.get(position).getItemsList(position), getActivity());
        mRecyclerView.setAdapter(mAdapter);
        processOrder = (Button)dialog.findViewById(R.id.processOrderBtn);
        edit=(Button) dialog.findViewById(R.id.editOrderBtn);
        delete=(Button) dialog.findViewById(R.id.deleteOrderBtn);

        space=(Space)dialog.findViewById(R.id.txtspace);
        sProcessClose=(Space)dialog.findViewById(R.id.sProcessClose);
        txtComments=(TextView)dialog.findViewById(R.id.comments);
        processOrder.setVisibility(View.GONE);
        edit.setVisibility(View.GONE);
        delete.setVisibility(View.GONE);


        space.setVisibility(View.GONE);
        sProcessClose.setVisibility(View.GONE);
        txtComments.setVisibility(View.VISIBLE);
        txtComments.setMovementMethod(new ScrollingMovementMethod());
        txtComments.setText(getString(R.string.textview_comments) + " " + cancelledOrderArrayList.get(position).getComment());

        close = (Button)dialog.findViewById(R.id.closeBtn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                // callBackListener.onClose();
            }
        });
        dialog.show();
    }
    public class GetCancelledOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response;
        Boolean showProgressDialog;

        public GetCancelledOrderDetails(Boolean showProgressDialog)
        {
            this.showProgressDialog = showProgressDialog;
        }


        @Override
        protected void onPreExecute() {
            if(showProgressDialog)
            {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String cancelledOrderURL = master.getCancelledOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(cancelledOrderURL, null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if(showProgressDialog)
            {
                if(pd != null && pd.isShowing())
                    pd.dismiss();
            }


            if(CancelledOrderFragment.this.isAdded()){

            }
            if (response.equals("exception"))
            {
                Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
            }
            else
            {
                try
                {

                    orderObjects= new ArrayList<JSONObject>();
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("orders");
                    if(jsonArray.length() == 0)
                    {
                        mRecyclerView.setVisibility(View.GONE);
                        tOrders.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        tOrders.setVisibility(View.GONE);
                        ++count;
                        for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                        {
                            orderObjects.add((JSONObject)jsonArray.get(recyclerViewIndex));
                        }
                        cancelledOrderArrayList = getOrders();
                        mAdapter = new OrderRecyclerViewAdapter(cancelledOrderArrayList, getActivity(),false);
                        if(count < 2) {
                            mRecyclerView.setAdapter(mAdapter);
                            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.cancelledOrderKey, CancelledOrderFragment.this));
                            mAdapter.notifyDataSetChanged();
                        }
                        else{
                            mRecyclerView.swapAdapter(mAdapter,true);
                        }
                    }
                    swipeContainer.setRefreshing(false);
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}

