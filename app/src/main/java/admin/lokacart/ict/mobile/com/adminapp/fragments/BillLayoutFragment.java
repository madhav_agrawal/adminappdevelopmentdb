package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.Validation;


/**
 * Created by root on 3/2/16.
 */
public class BillLayoutFragment  extends Fragment {

    EditText orgName,billHeader,billFooter,phoneNumber,address;
    String name, header, footer, phone, add;
    Button save;
    View rootView;
    Master master;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        rootView = inflater.inflate(R.layout.bill, container, false);
        getActivity().setTitle(R.string.title_settings);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        orgName = (EditText)rootView.findViewById(R.id.orgName);
        address = (EditText)rootView.findViewById(R.id.address);
        billHeader = (EditText)rootView.findViewById(R.id.billHeader);
        billFooter = (EditText)rootView.findViewById(R.id.billFooter);
        phoneNumber = (EditText)rootView.findViewById(R.id.contact2);
        save = (Button)rootView.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null && Master.isNetworkAvailable(getActivity())) {
                if(name.equals(orgName.getText().toString().trim())
                        && add.equals(address.getText().toString().trim())
                        && header.equals(billHeader.getText().toString().trim())
                        && footer.equals(billFooter.getText().toString().trim())
                        && phone.equals(phoneNumber.getText().toString().trim()))
                    Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                else if (checkValidation())
                    updateBill();
                else
                    Toast.makeText(getActivity(), R.string.label_toast_Form_contains_error, Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(getActivity()!=null && Master.isNetworkAvailable(getActivity())){
            new GetBillTask(getActivity()).execute();
        }

    }

    private void updateBill() {
        JSONObject profileObj = new JSONObject();
        try {
            profileObj.put("newname", orgName.getText().toString().trim());
            profileObj.put("newaddress", address.getText().toString().trim());
            profileObj.put("newcontact", phoneNumber.getText().toString().trim());
            profileObj.put("newheader", billHeader.getText().toString().trim());
            profileObj.put("newfooter", billFooter.getText().toString().trim());
            if(Master.isNetworkAvailable(getActivity())){
                new UpdateBillTask(getActivity()).execute(profileObj);
            }
            else{
                Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//-----------------------------get profile task---------------------------------------------------------------

    public class  GetBillTask extends AsyncTask<String,String, String> {

        ProgressDialog pd;
        Context context;
        String response;
        public GetBillTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String billURL = master.serverURL + "api/" + AdminDetails.getAbbr()+"/billsettingsview";
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(billURL,null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response1) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(BillLayoutFragment.this.isAdded())
            {
                if(response1.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later),getString(R.string.label_alertdialog_ok));
                }
                else
                {
                    try {
                        JSONObject resObj = new JSONObject(response1);
                        if(resObj.getString("response").equals("success")) {
                            orgName.setText(resObj.getString("name"));
                            address.setText(resObj.getString("address"));
                            phoneNumber.setText(resObj.getString("contact"));
                            billHeader.setText(resObj.getString("header"));
                            billFooter.setText(resObj.getString("footer"));

                            name = resObj.getString("name");
                            add = resObj.getString("address");
                            phone = resObj.getString("contact");
                            header = resObj.getString("header");
                            footer = resObj.getString("footer");
                        }
                        else {
                            Toast.makeText(getActivity(), resObj.getString("Error")+R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }


        }
    }


//----------------------------------------------------------------------------------------------------------




//-----------------------------------update profile---------------------------------------------------

    public class  UpdateBillTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String response;
        JSONObject jsonObject;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;

        public UpdateBillTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            jsonObject = params[0];
            String profileURL = master.serverURL + "api/" + AdminDetails.getAbbr()+"/billsettingsupdate";
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(profileURL, params[0], "POST",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response1) {
            if(pd != null && pd.isShowing())
            pd.dismiss();

            if(BillLayoutFragment.this.isAdded())
            {
                if(response1.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later), "OK");
                }
                else
                {
                    try {
                        JSONObject resObj = new JSONObject(response1);
                        if(resObj.getString("response").equals("Successfully updated"))
                        {
                            Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_bill_layout_has_been_updated_successfully), getString(R.string.label_alertdialog_ok));
                            if(AdminDetails.getName() != jsonObject.get("newname"))
                            {
                                AdminDetails.setName(jsonObject.getString("newname"));
                                ArrayList<TextView> textViews = DashboardActivity.getDrawerTextViews();
                                textViews.get(0).setText(jsonObject.getString("newname"));
                                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                                editor = sharedPreferences.edit();
                                editor.putString("name", AdminDetails.getName());
                                editor.commit();

                                name = jsonObject.getString("newname");
                                add = jsonObject.getString("newaddress");
                                phone = jsonObject.getString("newcontact");
                                header = jsonObject.getString("newheader");
                                footer = jsonObject.getString("newfooter");
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), resObj.getString("Error")+ R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

//------------------------------------------------------------------------------------------------------

    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(getActivity(), orgName)) ret = false;
        if (!Validation.isMobileNumber(getActivity(), phoneNumber, true, "mobileno"))ret=false;
        if (!Validation.hasText(getActivity(), address)) ret = false;
        if (!Validation.hasText(getActivity(), billHeader)) ret = false;
        if (!Validation.hasText(getActivity(), billFooter)) ret = false;
        return ret;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}