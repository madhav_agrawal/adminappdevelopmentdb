package admin.lokacart.ict.mobile.com.adminapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnBackPressListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

import admin.lokacart.ict.mobile.com.adminapp.fragments.ProductTypeFragment;

/**
 * Created by Vishesh on 04-01-2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.DataObjectHolder> implements ItemTouchHelperAdapter
{
    static String LOG_TAG = "RecyclerViewAdapter";
   // ArrayList<String> mDataset;
    Context context;


    TextView textView;

    private int currentposition;
    private boolean flag;
    private ProductTypeFragment productTypeFragment;


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }


    public int getCurrentPosition() {
        return currentposition;
    }

    public void setCurrentPosition(int currentposition) {
        this.currentposition = currentposition;
    }


    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setColorWhite(TextView textView){
        textView.setBackgroundColor(Color.WHITE);
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        public TextView gettDisplayText() {
            return tDisplayText;
        }

        public final TextView tDisplayText,tDisplayStatusText;
        MyListener callback;


        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            tDisplayText = (TextView) itemView.findViewById(R.id.tDisplayText);
            tDisplayStatusText = (TextView) itemView.findViewById(R.id.tDisplayStatusText);

            setTextView(gettDisplayText());
            callback = (MyListener) context;
            tDisplayText.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {

            DashboardActivity.fab.setVisibility(View.GONE);

            if (getCurrentPosition() == getAdapterPosition() && isFlag()==false) {
                tDisplayText.setBackgroundColor(Color.LTGRAY);
                Master.backCheck=1;
                setCurrentPosition(getAdapterPosition());
                productTypeFragment.editInvisible(true);
                setFlag(true);

            } else if(getCurrentPosition() == getAdapterPosition() && isFlag()==true){
                tDisplayText.setBackgroundColor(Color.WHITE);
                Master.backCheck=0;
                productTypeFragment.editInvisible(false);
                DashboardActivity.fab.setVisibility(View.VISIBLE);
                setFlag(false);

            } else if(getCurrentPosition() != getAdapterPosition() && isFlag()==false){
                tDisplayText.setBackgroundColor(Color.LTGRAY);
                setCurrentPosition(getAdapterPosition());
                Master.backCheck=1;
                productTypeFragment.editInvisible(true);
                setFlag(true);

            }
            else{

            }


            return false;
        }
    }

   /* public RecyclerViewAdapter(ArrayList<String> myDataset, Context context, Fragment productTypeFragment) {
        mDataset = myDataset;
        this.context = context;
        this.productTypeFragment=(ProductTypeFragment) productTypeFragment;
        setFlag(false);

    }*/

    public RecyclerViewAdapter(Context context, Fragment productTypeFragment) {
      //  mDataset = myDataset;
        this.context = context;
        this.productTypeFragment=(ProductTypeFragment) productTypeFragment;
        setFlag(false);

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
       // holder.tDisplayText.setText(mDataset.get(position));
        holder.tDisplayText.setText(Master.productTypeDisplayList.get(position).getName());

        if(Master.productTypeDisplayList.get(position).productTypeStatus==1)
        {
            holder.tDisplayStatusText.setText("Enabled");
            holder.tDisplayStatusText.setTextColor(Color.GREEN);
        }
        else
        {
            holder.tDisplayStatusText.setText("Disabled");
            holder.tDisplayStatusText.setTextColor(Color.RED);

        }
        holder.tDisplayText.setLongClickable(true);
        if(position==getCurrentPosition() && isFlag()){
            holder.tDisplayText.setBackgroundColor(Color.LTGRAY);
        }
        else
            holder.tDisplayText.setBackgroundColor(Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return Master.productTypeDisplayList.size();
       // return mDataset.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition<getItemCount() && toPosition<getItemCount()) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    //Collections.swap(mDataset, i, i + 1);
                    Collections.swap(Master.productTypeDisplayList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                  //  Collections.swap(mDataset, i, i - 1);

                    Collections.swap(Master.productTypeDisplayList, i, i - 1);

                }
            }
            notifyItemMoved(fromPosition, toPosition);
        }
    }

    @Override
    public void onItemDismiss(int position) {
        if(position<getItemCount()+1) {
           // mDataset.remove(position);
            Master.productTypeDisplayList.remove(position);
            notifyItemRemoved(position);
        }
    }

}
