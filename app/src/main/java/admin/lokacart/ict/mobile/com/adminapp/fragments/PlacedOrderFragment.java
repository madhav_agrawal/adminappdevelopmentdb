package admin.lokacart.ict.mobile.com.adminapp.fragments;

/**
 * Created by root on 18/1/16.
 */
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.EditOrderActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.OrderDetailsRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.OrderRecyclerViewAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.RecyclerItemClickListener;
import admin.lokacart.ict.mobile.com.adminapp.SavedOrder;

public class PlacedOrderFragment extends Fragment {
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    protected LayoutManagerType mCurrentLayoutManagerType;

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    //ArrayList<SavedOrder> savedOrderArrayList;
    View savedOrderFragmentView;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    public static RecyclerView.Adapter orderAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Master master;
    JSONObject responseObject;
    static int recyclerViewIndex;
    ArrayList<JSONObject> orderObjects;
    ArrayList<SavedOrder> orders;
    private SwipeRefreshLayout swipeContainer;
    int count = 0;
    TextView tOrders;
    String stockEnabledStatus = "false";
    String minimumBillAmount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //setHasOptionsMenu(true);

        savedOrderFragmentView = inflater.inflate(R.layout.orders_recycler_view, container, false);
        getActivity().setTitle(R.string.title_orders);

        return savedOrderFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Master.savedOrderArrayList = new ArrayList<SavedOrder>();
    }

    public ArrayList<SavedOrder> getOrders() {

        orders = new ArrayList<SavedOrder>();
        int count = 0;
        if (orderObjects.size() != 0) {
            for (JSONObject entry : orderObjects) {
                SavedOrder order = new SavedOrder(entry, count, Master.PLACEDORDER);
                orders.add(order);
                count++;
            }
        } else {
        }
        return orders;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new GetSavedOrderDetails(true).execute();


        mRecyclerView = (RecyclerView) savedOrderFragmentView.findViewById(R.id.savedOrderRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        swipeContainer = (SwipeRefreshLayout) savedOrderFragmentView.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetSavedOrderDetails(false).execute();
            }
        });
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);

        tOrders = (TextView) savedOrderFragmentView.findViewById(R.id.tOrder);
        tOrders.setText(R.string.label_no_pending_orders_present);
        tOrders.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
    }


    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {

            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void clickListener(final int position) {
        RecyclerView mRecyclerView;

        RecyclerView.LayoutManager mLayoutManager;

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.orders_details_list))
                .setGravity(Gravity.CENTER)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(false)
                .setCancelable(true)
                .create();

        //try{

        //} catch (Exception e){
        //  e.printStackTrace();
        //}

        Button processOrder, close, edit, delete;
        Space space;
        TextView txtComments;

        mRecyclerView = (RecyclerView) dialog.findViewById(R.id.orderdetailsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        orderAdapter = new OrderDetailsRecyclerViewAdapter(Master.savedOrderArrayList.get(position).getPlacedOrderItemsList(position), getActivity());
        mRecyclerView.setAdapter(orderAdapter);
        processOrder = (Button) dialog.findViewById(R.id.processOrderBtn);
        space = (Space) dialog.findViewById(R.id.txtspace);
        txtComments = (TextView) dialog.findViewById(R.id.comments);

        close = (Button) dialog.findViewById(R.id.closeBtn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        processOrder.setVisibility(View.VISIBLE);
        space.setVisibility(View.VISIBLE);
        txtComments.setVisibility(View.GONE);
        processOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ChangeSavedToProcessedOrder(dialog, position).execute("" + Master.savedOrderArrayList.get(position).getOrderId());
            }
        });

        edit = (Button) dialog.findViewById(R.id.editOrderBtn);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditOrderActivity.class);
                intent.putExtra("selectedOrder", (Serializable) Master.savedOrderArrayList.get(position));
                intent.putExtra("position", position);
                intent.putExtra("stockEnabledStatus", stockEnabledStatus);
                intent.putExtra("orderId", Master.savedOrderArrayList.get(position).getOrderId());
                intent.putExtra("minimumBillAmount", minimumBillAmount);
                startActivity(intent);
                dialog.dismiss();
            }
        });


        delete = (Button) dialog.findViewById(R.id.deleteOrderBtn);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new DeleteOrder(dialog,position).execute("" + Master.savedOrderArrayList.get(position).getOrderId());

            }
        });


        dialog.show();

    }

    //-----------------------------------------Class for change saved to processed orders-------------------------------


    public class ChangeSavedToProcessedOrder extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response;
        DialogPlus d;
        int pos;

        public ChangeSavedToProcessedOrder(DialogPlus dialogPlus, int position) {
            d = dialogPlus;
            pos = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String changeSavedToProcessedOrderURL = master.getChangeSavedToProcessedOrderURL() + Integer.valueOf(params[0]);
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(changeSavedToProcessedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (pd != null && pd.isShowing())
                pd.dismiss();

            if (PlacedOrderFragment.this.isAdded()) {
                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else {
                    try {
                        responseObject = new JSONObject(response);
                        if (responseObject.getString("result").equals("success")) {
                            Toast.makeText(getActivity(), R.string.label_toast_Change_saved_to_processed_order, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("order already processed")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        }else if (responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("order already cancelled")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        }
                        else if(responseObject.getString("result").equals("failure") && responseObject.getString("error").equals("Order has already been deleted")){
                            //System.out.println("Hi i am here");
                            Toast.makeText(getActivity(), getString(R.string.label_toast_processing_of_deleted_order), Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                    }
                }
            }

        }
    }

    //----------------------------------------------------------------------------------------------------------------------
    public class GetSavedOrderDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response;
        Boolean showProgressDialog;

        public GetSavedOrderDetails(Boolean showProgressDialog) {
            this.showProgressDialog = showProgressDialog;
        }


        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                pd = new ProgressDialog(getActivity());
                pd.setMessage(getString(R.string.pd_loading_orders));
                pd.setCancelable(false);
                pd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            String savedOrderURL = master.getSavedOrderURL() + "?orgabbr=" + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(savedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (showProgressDialog) {
                if (pd != null && pd.isShowing())
                    pd.dismiss();
            }

            if (PlacedOrderFragment.this.isAdded()) {
                if (response.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else {
                    try {

                        orderObjects = new ArrayList<JSONObject>();
                        responseObject = new JSONObject(response);
                        stockEnabledStatus = responseObject.getString("stockManagement");
                        // minimumBillAmount="150"; // hard coded for testing purposes
                        minimumBillAmount = "" + responseObject.getInt("minimumBillOrder");
                        JSONArray jsonArray = responseObject.getJSONArray("orders");
                        if (jsonArray.length() == 0) {
                            mRecyclerView.setVisibility(View.GONE);
                            tOrders.setVisibility(View.VISIBLE);
                        } else {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            tOrders.setVisibility(View.GONE);
                            ++count;
                            for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex) {
                                orderObjects.add((JSONObject) jsonArray.get(recyclerViewIndex));
                            }
                            Master.savedOrderArrayList = getOrders();
                            mAdapter = new OrderRecyclerViewAdapter(Master.savedOrderArrayList, getActivity(), false);
                            if (count < 2) {
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, Master.savedToProcessedKey, PlacedOrderFragment.this));
                                mAdapter.notifyDataSetChanged();
                            } else {
                                mRecyclerView.swapAdapter(mAdapter, true);
                            }
                        }

                        swipeContainer.setRefreshing(false);
                    } catch (Exception e) {

                    }
                }
            }

        }
    }

//----------------------deleting order--------------------------------------------------------------------------------

    public class DeleteOrder extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        String response;
        DialogPlus d;
        int pos;

        public DeleteOrder(DialogPlus dialogPlus,int position) {

            pos = position;
            d= dialogPlus;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            // String url="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+params[0];

            JSONObject obj = new JSONObject();
            try {
//                Log.e("CAncel order", "in doinBackGround");
                obj.put("status", "deleted");
                obj.put("comments", "cancelled by admin");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String deleteOrderURL = master.getDeleteOrderURL(Integer.valueOf(params[0]));
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(deleteOrderURL, obj, "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;


        }

        protected void onPostExecute(String response1) {


            if (pd != null && pd.isShowing())
                pd.dismiss();

            if (PlacedOrderFragment.this.isAdded()) {
                if (response1.equals("exception")) {
                    Master.alertDialog(getActivity(), getString(R.string.label_cannot_connect_to_the_server), getString(R.string.label_alertdialog_ok));
                } else {
                    try {
                        responseObject = new JSONObject(response);
                        if (responseObject.getString("status").equals("Success")) {
                            Toast.makeText(getActivity(), R.string.label_toast_delete_order, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("status").equals("error") && responseObject.getString("error").equals("Order has already been deleted by you")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else if (responseObject.getString("status").equals("error") && responseObject.getString("error").equals("Order has already been deleted the User")) {
                            Toast.makeText(getActivity(), R.string.label_toast_order_cancel_reupdate, Toast.LENGTH_SHORT).show();
                            d.dismiss();
                            Master.savedOrderArrayList.remove(pos);
                            mAdapter.notifyItemRemoved(pos);
                            recyclerViewIndex--;
                            if (recyclerViewIndex == 0) {
                                tOrders.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                tOrders.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

        }

    }
}
