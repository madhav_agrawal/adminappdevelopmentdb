package admin.lokacart.ict.mobile.com.adminapp;
import android.content.Context;
import android.widget.EditText;

public class Validation {

    private static final String REQUIRED_MSG = "required";
    private static final String EMAIL_MSG = R.string.label_toast_please_enter_a_valid_email_id+"";
    private static final String PASSWORD_MSG = "Password does not match";
    private static final String MOBILE_MSG = R.string.label_toast_please_enter_a_valid_mobile_number+"";
    public static boolean isEmailAddress(Context ctx,EditText editText, boolean required,String field) {
        return isValid(ctx,editText, EMAIL_MSG, required,field);
    }

    public static boolean isMobileNumber(Context ctx,EditText editText, boolean required,String field) {
        return isValid(ctx,editText, MOBILE_MSG, required,field);
    }
 
    public static boolean isValid(Context ctx,EditText editText, String errMsg, boolean required,String field) {
 
        String text = editText.getText().toString().trim();
        editText.setError(null);
 
        if ( required && !hasText(ctx,editText) ) return false;

        if(field.equals("emailid"))
        {
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches())
            {
                editText.setError(ctx.getString(R.string.label_validation_email));
                return false;
            }
        }
        else if(field.equals("mobileno"))
        {
            long i = Long.parseLong(text);
            long length = (long)(Math.log10(i)+1);
            if(length<10 || length > 10){
                editText.setError(ctx.getString(R.string.label_validation_mobile));
                return false;
            }
        }
        return true;
    }

    public static boolean isValid(Context ctx,EditText editText, String firstPassword, String errMsg, boolean required) {
 
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if ( required && !hasText(ctx,editText) ) return false;
        if(!firstPassword.equals(text))
        {
            editText.setError(ctx.getString(R.string.label_validation_password));
            return false;
        }
        return true;
    }
 
    public static boolean isPasswordMatch(Context ctx,EditText editText,String firstPassoword, boolean required) {
        return isValid(ctx,editText, firstPassoword,ctx.getString(R.string.label_validation_password), required);
    }

    public static boolean hasText(Context ctx,EditText editText) {
 
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {
            editText.setError(ctx.getString(R.string.label_validation_required));
            return false;
        }
        return true;
    }
}