package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 6/6/16.
 */
public class BroadcastMessage extends Fragment {

    View broadcastMessageView;

    public EditText eBroadcastEditText;
    public Button bBroadcastButton;
    public TextView tCharRemaining;
    public RadioButton rSMS,rPushAsNotification;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        broadcastMessageView = inflater.inflate(R.layout.fragment_broadcast_message,container,false);
        
        getActivity().setTitle(R.string.title_broadcast_notification);

        setHasOptionsMenu(true);

        eBroadcastEditText = (EditText) broadcastMessageView.findViewById(R.id.eBroadcastMessage);
        bBroadcastButton = (Button) broadcastMessageView.findViewById(R.id.bBroadcastSubmit);
        tCharRemaining = (TextView) broadcastMessageView.findViewById(R.id.tCharRemaining);
        rSMS=(RadioButton) broadcastMessageView.findViewById(R.id.rSMS);
        rSMS.setChecked(true);
        rPushAsNotification=(RadioButton) broadcastMessageView.findViewById(R.id.rPushAsNotification);
        rPushAsNotification.setChecked(false);


        if(BroadcastMessage.this.isAdded() && getActivity()!=null)
        ((DashboardActivity)getActivity()).updateStatusBarColor();


        eBroadcastEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    try {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        eBroadcastEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != -1)
                    tCharRemaining.setText((100 - s.length()) + " " + getString(R.string.textview_characters_left));
            }
        });

        bBroadcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Close the keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(broadcastMessageView.getWindowToken(),0);


                String message= eBroadcastEditText.getText().toString().trim();

                if(message.equals("") || message.isEmpty()){
                    Toast.makeText(getActivity(),getString(R.string.label_toast_broadcast_message_request), Toast.LENGTH_SHORT).show();
                }
                else if(message.length()> 100){
                    Toast.makeText(getActivity(),getString(R.string.label_toast_reduce_message_length),Toast.LENGTH_SHORT).show();
                }
                else if(getActivity()!=null && !Master.isNetworkAvailable(getActivity())){
                    Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                }
                else {
                    final JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("orgabbr", AdminDetails.getAbbr());
                        //Log.e("Admin organisation",AdminDetails.getEmail());
                        jsonObject.put("message", message);
                    } catch (JSONException e) {
                    }
                    if(rPushAsNotification.isChecked()) {
                        new SendNotification(getActivity(),1).execute(jsonObject);
                    }
                    else
                    {
                        // Call the API for SMS
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle(getString(R.string.label_button_confirm));
                        alertDialog.setMessage(getString(R.string.dialog_SMS_confirm_message));
                        alertDialog.setPositiveButton(getString(R.string.label_alertdialog_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new SendNotification(getActivity(),0).execute(jsonObject);
                            }
                        });
                        alertDialog.setNegativeButton(getString(R.string.label_button_cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();

                    }
                }
            }
        });

        return broadcastMessageView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }



  /*  ------------------------------------API to send notification---------------------------------*/

    public class SendNotification extends AsyncTask<JSONObject,String,String>{

        ProgressDialog progressDialog;
        Context context;
        Master master;
        String response;
        int option;

        SendNotification(Context context,int option){

            this.context=context;
            this.option=option;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.label_please_wait));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            master= new Master();
            GetJSON getJSON = new GetJSON();
            if(option==1) { //Push as Notification
                response = getJSON.getJSONFromUrl(master.getBroadcastURL(), (JSONObject) params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            }
            else{
                //Call the URL for SMS
                response = getJSON.getJSONFromUrl(master.getSendSMSURL(),(JSONObject) params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            }
            return response;

        }

        @Override
        protected void onPostExecute(String response) {

            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if (BroadcastMessage.this.isAdded()) {

                try {
                    JSONObject responseObject = new JSONObject(response);
                    response = responseObject.getString("response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (response.equals(getString(R.string.label_toast_message_successfully_broadcasted))) {
                    Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                } else if(response.equals("Message successfully broadcasted")){
                    Toast.makeText(getActivity(),getString(R.string.label_toast_message_successfully_broadcasted),Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), getString(R.string.label_toast_error), Toast.LENGTH_SHORT).show();
                }

            }
        }


    }

}
