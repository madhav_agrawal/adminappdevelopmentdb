package admin.lokacart.ict.mobile.com.adminapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.TreeMap;

public class SavedOrder implements Serializable{

    String userName;
    int orderId;
    String timeStamp;
    String comment;
    double totalBill;
    int quantity=1;
    TreeMap<String,String[]> placedOrderItemLists;
    HashSet<String> set;

    public String getStockEnabledStatus() {
        return stockEnabledStatus;
    }

    public void setStockEnabledStatus(String stockEnabledStatus) {
        this.stockEnabledStatus = stockEnabledStatus;
    }

    String stockEnabledStatus;


    String payment;
    TreeMap<String,String[]> itemList;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getPayment() { return payment; }

    public void setPayment(String payment) { this.payment = payment; }

    public SavedOrder(JSONObject order, int pos, String type)
    {

        if(type.equals(Master.PLACEDORDER)){

            try {
                this.totalBill=0.0;
                this.orderId=order.getInt("orderid");
                this.userName = order.getString("username");
                this.timeStamp=order.getString("timestamp");
               // this.stockEnabledStatus=order.getString("stockManagement");
                JSONArray orderItems=order.getJSONArray("items");
                this.placedOrderItemLists =new TreeMap<String, String[]>();
                this.set= new HashSet<>();
//                System.out.println("------"+order_id+"-------"+timeStamp+"------"+stockEnabledStatus);
                for(int i=  0;i<orderItems.length();i++){
                    JSONObject item=(JSONObject)orderItems.get(i);
                    String name=item.getString("productname");
                    String[] temp=new String[3];
                    temp[0]=item.getString("rate");
                    quantity=Integer.parseInt(item.getString("quantity"));
                    if(!set.contains(name))
                    {
                        set.add(name);
                        temp[1]=item.getString("quantity");
                    }
                    else
                    {
                        temp[1]=String.valueOf(Integer.parseInt(this.placedOrderItemLists.get(pos + name)[1]) + Integer.parseInt(item.getString("quantity")));
                    }

                    temp[2]=item.getString("stockQuantity");
                    this.placedOrderItemLists.put(pos + name, temp);
                    this.totalBill=this.totalBill+(Double.parseDouble(String.format("%.2f",(Double.parseDouble(temp[0])*quantity))));

//                    System.out.println("------" + name + "-------" + temp[0] + "------" + temp[1] + "-----" + temp[2]);


                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        else {
            try {
                this.orderId = order.getInt("orderid");
                this.userName = order.getString("username");
                this.timeStamp = order.getString("timestamp");

                try {
                    this.payment = order.getString("paid");
                } catch (JSONException e) {

                }

                try {
                    this.comment = order.getString("comment");
                } catch (JSONException e) {
                    this.comment = "No Comments";
                    //this.payment="Unpaid";
                }
                JSONArray orderItems = order.getJSONArray("items");
                this.itemList = new TreeMap<String, String[]>();
                for (int i = 0; i < orderItems.length(); i++) {
                    JSONObject item = (JSONObject) orderItems.get(i);
                    String name = item.getString("productname");
                    String[] temp = new String[2];
                    temp[0] = item.getString("rate");
                    temp[1] = item.getString("quantity");
                    this.itemList.put(pos + name, temp);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getItems()
    {
    	String res="";
    	for(String itemName:itemList.keySet())
        {
    		res+= itemName + "-" + itemList.get(itemName)[1] + "\n";
    	}
    	return res;
    }

    public ArrayList<String[]> getItemsList(int pos)
    {
        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res = new String[3] ;
        int posLen = (""+pos).length();
        for (String itemName:itemList.keySet())
        {
            res = new String[3] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]=itemList.get(itemName)[0];
            res[2]=itemList.get(itemName)[1];
            double rate= Double.parseDouble(itemList.get(itemName)[0]);
            double quantity= Double.parseDouble(itemList.get(itemName)[1]);
            double stockQuantity;
            itemlist.add(res);
        }
        return itemlist;
    }

    public ArrayList<String[]> getPlacedOrderItemsList(int pos) {

        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res ;

        int posLen = (""+pos).length();


        for (String itemName: placedOrderItemLists.keySet())
        {

            res = new String[5] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]= placedOrderItemLists.get(itemName)[0];
            res[2]= placedOrderItemLists.get(itemName)[1];

            double rate=Double.parseDouble(placedOrderItemLists.get(itemName)[0]);
            int quantity=Integer.parseInt(placedOrderItemLists.get(itemName)[1]);
            //  double total = quantity * rate;
            double total = Double.parseDouble(String.format("%.2f",(quantity*rate)));

            res[3]= ""+total;
            res[4]= placedOrderItemLists.get(itemName)[2]; //this is stock quantity

//            System.out.println(res[0]+"------"+res[1]+"---------"+res[2]+"--------"+res[3]+"--------"+res[4]);


            itemlist.add(res);
            res=null;
        }

        return itemlist;
    }

}