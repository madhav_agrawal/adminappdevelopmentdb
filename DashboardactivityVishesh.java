package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 29/12/15.
 */
import admin.lokacart.ict.mobile.com.adminapp.fragments.SettingsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.OrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.MemberFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.DashboardFragment;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.*;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MyListener, NavigationItemListener, AlertDialogFragment.DialogListener {
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Master master;
    JSONObject responseObject;
    Dialog dialog;
    public static ArrayList productTypeArrayList;
    static int recyclerViewIndex;
    FragmentManager fragmentManager;
    static View productFragmentView;
    Bundle args = new Bundle();

    public static RecyclerView mRecyclerView;
    public static RecyclerView.Adapter mAdapter;
    public static RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "DashboardActivity";

    ProgressDialog progressDialog;

    TextView tDashboardMobileNumber, tDashboardName, tDashboardEmail;
    String newProductTypeName;
    boolean dashboardUpdate;
 Fragment fragment = null;
    public String dashboard_TAG;
    public static Boolean backPress;
    private final String TAG = "DashboardActivity";
    NavigationView navigationView;
    FloatingActionButton fab;
AlertDialogFragment dialogFragment;
    Class fragmentClass;
    FragmentManager fm = getSupportFragmentManager();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    public static void resetBackPress()
    {
        backPress = false;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(MyGcmListenerService.ACTION));
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor;
        Log.d(TAG,"in onResume");
        if(sharedPrefs.contains("orderNotifs"))
        {
            Log.d(TAG,"deleting orderNotifs");
            editor = sharedPrefs.edit();
            editor.remove("orderNotifs");
            editor.apply();
        }
        if(sharedPrefs.contains("memberNotifs"))
        {
            Log.d(TAG,"deleting memberNotifs");
            editor = sharedPrefs.edit();
            editor.remove("memberNotifs");
            editor.apply();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public Context getContext()
    {
        return DashboardActivity.this;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        dashboard_TAG = "DASHBOARD_TAG";
        Log.d(LOG_TAG, "Starting Dashboard Activity");
        backPress = false;
        dashboardUpdate = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        Intent intent = new Intent(this, RegistrationIntentService.class);
        if (checkPlayServices())
        {
            startService(intent);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(MyGcmListenerService.ACTION)) {

                    try
                    {
                        int params[] = new int[6];
                        params[0] = Integer.parseInt(intent.getStringExtra("saved"));
                        params[1] = Integer.parseInt(intent.getStringExtra("processed"));
                        params[2] = Integer.parseInt(intent.getStringExtra("cancelled"));
                        params[3] = Integer.parseInt(intent.getStringExtra("totalUsers"));
                        params[4] = Integer.parseInt(intent.getStringExtra("newUsersToday"));
                        params[5] = Integer.parseInt(intent.getStringExtra("pendingUsers"));

                        DashboardFragment dashboardFragment = (DashboardFragment) getSupportFragmentManager().findFragmentByTag(dashboard_TAG);
                        if(dashboardFragment != null && dashboardFragment.isVisible())
                        {
                            Log.e("Vishesh", params[0] + " " + params[1] + " " + params[2] + " " + params[3] + " " + params[4] + " " + params[5]);
                            System.out.println(params);
                            dashboardFragment.update(params);
                        }
                        else
                        {
                            Log.e("Vish", "In push message else part Dashboard fragment not active1");
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        productTypeArrayList = new ArrayList<String>(); // for storing the fetched product types
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();
        master = new Master();

        tDashboardEmail = (TextView) findViewById(R.id.tDashboardEmail);
        tDashboardEmail.setText(AdminDetails.getEmail());

        tDashboardName = (TextView) findViewById(R.id.tDashboardName);
        tDashboardName.setText(AdminDetails.getName());

        tDashboardMobileNumber = (TextView) findViewById(R.id.tDashboardMobileNumber);
        tDashboardMobileNumber.setText(AdminDetails.getMobileNumber());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(LOG_TAG, "FAB clicked");
                addNewProductType();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            getSupportFragmentManager().popBackStack();
        }
        else if(backPress)
        {
            finish();
        }
        else
        {
            Toast.makeText(DashboardActivity.this, R.string.label_toast_press_back_once_more_to_exit, Toast.LENGTH_SHORT).show();
            backPress = true;
        }
    }


    /*  The options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }

    //----------Method to clear the fragment backstack----------------
    private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        resetBackPress();
        clearBackStack();

        int id = item.getItemId();

        if(id == R.id.nav_dashboard) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(0).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
            dashboardUpdate = false;

        } else if (id == R.id.nav_products) {
            fab.setVisibility(View.VISIBLE);
            productTypeArrayList.clear();
            navigationView.getMenu().getItem(1).setChecked(true);
            new GetAdminAndProductTypeDetails(getApplicationContext()).execute();
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductFragment()).addToBackStack("dashboard").commit();

        } else if (id == R.id.nav_orders) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(2).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new OrderFragment()).commit();

        } else if (id == R.id.nav_members) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(3).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MemberFragment()).commit();

        } else if (id == R.id.nav_settings) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(4).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SettingsFragment()).commit();

        } else if (id == R.id.nav_logout) {

            navigationView.getMenu().getItem(5).setChecked(true);
            editor.putBoolean("login", false);
            editor.commit();
            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put("token", Master.getToken());
                System.out.println(jsonObject);
            }
            catch (JSONException e)
            {
                Log.e("DASHBOARD", "De registering error");
            }
            new GCMDeregisterTask(DashboardActivity.this).execute(jsonObject);
            startActivity(new Intent(DashboardActivity.this, MainActivity.class));
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void itemSelected(int itemIndex)
    {
        navigationView.getMenu().getItem(itemIndex).setChecked(true);
    }

    public ArrayList<String> getDataSet() {
        if (Master.isNetworkAvailable(DashboardActivity.this)) {
            try {
                Log.d("before calling funtion", "Vish");
            } catch (Exception e) {
                Log.e(LOG_TAG, "While populating cards :" + e.getMessage());
            }
        } else {
            Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_internet), "OK");
        }
        return productTypeArrayList;
    }



    public void addNewProductType() {

        dialog = new Dialog(DashboardActivity.this);
        dialog.setContentView(R.layout.product_type_box);
        dialog.setTitle(R.string.dialog_title_add_product_type);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductTypeName, eProductTypeUnit;
        final Button bProductTypeConfirm, bProductTypeCancel, bProductTypeDelete, bProductTypeEdit;

        eProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
        eProductTypeName.setHint(R.string.hint_enter_new_product_type);

        eProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
        eProductTypeUnit.setHint(R.string.hint_enter_unit);


        bProductTypeEdit = (Button) dialog.findViewById(R.id.bProductTypeEdit);
        bProductTypeEdit.setVisibility(View.GONE);
        bProductTypeDelete = (Button) dialog.findViewById(R.id.bProductTypeDelete);
        bProductTypeDelete.setVisibility(View.GONE);

        bProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
        bProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

        bProductTypeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO send the new product name
                newProductTypeName = eProductTypeName.getText().toString().trim() + " ( " +
                        eProductTypeUnit.getText().toString().trim() + " )";

                if (eProductTypeName.getText().toString().equals("")) {
                    Toast.makeText(DashboardActivity.this, R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                } else if (eProductTypeUnit.getText().toString().equals("")) {
                    Toast.makeText(DashboardActivity.this, R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                } else if (!Master.isNetworkAvailable(DashboardActivity.this)) {
                    Toast.makeText(DashboardActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("Vish ", "In final else");

                    boolean flag = true;
                    for (int i = 0; i < recyclerViewIndex; ++i) {
                        if (productTypeArrayList.get(i).equals(newProductTypeName)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", newProductTypeName);
                        } catch (JSONException e) {
                            Log.e("JSONerror in Dashboard ", " while adding product type");
                            System.out.println(e.getMessage());
                        }

                        new MyAsyncTask(DashboardActivity.this, newProductTypeName).execute(jsonObject);

                    } else {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_please_enter_unique_product_type, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onAsyncTaskComplete(String response, int flag) {
        switch (flag) {
            case 1:
                if (response.equals("exception")) {
                    master.alertDialog(DashboardActivity.this, "Cannot connect to the server!", "OK");
                } else {
                    try {
                        responseObject = new JSONObject(response);
                    } catch (JSONException e) {
                        System.out.println(e.getMessage());
                    }
                    try {
                        response = responseObject.getString("upload");
                        if (response.equals("success")) {
                            Toast.makeText(DashboardActivity.this, R.string.label_toast_Product_type_added_successfully, Toast.LENGTH_SHORT).show();
                            productTypeArrayList.add(recyclerViewIndex++, newProductTypeName);
                            System.out.println(productTypeArrayList);
                            mAdapter.notifyItemInserted(recyclerViewIndex - 1);
                            mAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        System.out.println(e.getMessage());
                    }
                }
                break;

        }
    }

    @Override
    public void onCardClickListener(int position,int cat,Object obj) {
       // Toast.makeText(DashboardActivity.this, (CharSequence) productTypeArrayList.get(position), Toast.LENGTH_SHORT).show();
        Log.e("Vish", "in oncardclicklistener");
          args.clear();
        if(cat==2||cat==4)
        {
            System.out.println("------------------oncraddddddddddddddddddddddddddddddddddddddd--------------------------");

    ;

            ArrayList<SavedOrder> savedOrderArrayList = (ArrayList<SavedOrder>)obj;

            SavedOrder order = savedOrderArrayList.get(position);

            args.putInt("ID", 1);

            args.putString("MESSAGE", "Process Order");
            args.putInt("position", position);
            args.putInt("category", cat);
            args.putSerializable("detailslist", order.getItemsList(position));
            args.putInt("orderid", order.getOrderId());
            args.putString("comment", order.getComment());
            dialogFragment = new AlertDialogFragment();
            dialogFragment.setCancelable(false);
            dialogFragment.show(getSupportFragmentManager(), "Saved-order");
            dialogFragment.setArguments(args);
        }
        else
        {
            if(cat==3)
            {

                ArrayList<SavedOrder> savedOrderArrayList = (ArrayList<SavedOrder>)obj;

                SavedOrder order = savedOrderArrayList.get(position);
                new ViewBillAsyncTask(this).execute(""+order.getOrderId());
            }
            else
            {
                Intent productIntent = new Intent(DashboardActivity.this, ProductActivity.class);
                productIntent.putExtra("product type", (String) productTypeArrayList.get(position));
                startActivity(productIntent);
            }

        }

    }

    @Override
    public void onCardLongClickListener(final int position)
    {
        final EditText eEditProductTypeName, eEditProductTypeUnit;
        final Button bEditProductTypeConfirm, bEditProductTypeCancel, bEditProductTypeDelete, bEditProductTypeEdit;

        final String selectedProductType = (String) productTypeArrayList.get(position);

        if(Master.isNetworkAvailable(DashboardActivity.this))
        {

            dialog = new Dialog(DashboardActivity.this);
            dialog.setContentView(R.layout.product_type_box);
            dialog.setTitle("Modify " + selectedProductType);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            eEditProductTypeName = (EditText) dialog.findViewById(R.id.eProductTypeName);
            eEditProductTypeName.setVisibility(View.GONE);

            eEditProductTypeUnit = (EditText) dialog.findViewById(R.id.eProductTypeUnit);
            eEditProductTypeUnit.setVisibility(View.GONE);

            bEditProductTypeConfirm = (Button) dialog.findViewById(R.id.bProductTypeConfirm);
            bEditProductTypeConfirm.setVisibility(View.GONE);

            bEditProductTypeEdit = (Button) dialog.findViewById(R.id.bProductTypeEdit);
            bEditProductTypeDelete = (Button) dialog.findViewById(R.id.bProductTypeDelete);
            bEditProductTypeCancel = (Button) dialog.findViewById(R.id.bProductTypeCancel);

            bEditProductTypeCancel.setOnClickListener(new View.OnClickListener()
                                                      {
                                                          @Override
                                                          public void onClick(View v)
                                                          {
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeDelete.setOnClickListener(new View.OnClickListener()
                                                      {
                                                          @Override
                                                          public void onClick(View v)
                                                          {
                                                              JSONObject jsonObject = new JSONObject();
                                                              try {
                                                                  jsonObject.put("orgabbr", AdminDetails.getAbbr());
                                                                  jsonObject.put("name", selectedProductType);
                                                              } catch (JSONException e) {
                                                                  Log.e("Del prod type", "Json creation exception : " + e.getMessage());
                                                              }
                                                              new DeleteProductTypeTask(position).execute(jsonObject);
                                                              dialog.dismiss();
                                                          }
                                                      }
            );

            bEditProductTypeEdit.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            bEditProductTypeDelete.setVisibility(View.GONE);
                                                            bEditProductTypeEdit.setVisibility(View.GONE);

                                                            eEditProductTypeName.setVisibility(View.VISIBLE);
                                                            eEditProductTypeUnit.setVisibility(View.VISIBLE);
                                                            bEditProductTypeConfirm.setVisibility(View.VISIBLE);

                                                            int indexOfBracket = selectedProductType.indexOf('(');
                                                            String pName, pUnit;
                                                            pName = selectedProductType.substring(0, indexOfBracket - 1);
                                                            pUnit = selectedProductType.substring(indexOfBracket + 2, selectedProductType.length()-2);
                                                            eEditProductTypeName.setText(pName);
                                                            eEditProductTypeUnit.setText(pUnit);
                                                        }
                                                    }
            );

            bEditProductTypeConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(eEditProductTypeName.getText().toString().equals(""))
                    {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_please_enter_a_product_type, Toast.LENGTH_SHORT).show();
                    }
                    else if(eEditProductTypeUnit.getText().toString().equals(""))
                    {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_please_enter_a_unit, Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        String updatedProductType = eEditProductTypeName.getText().toString().trim() + " ( " + eEditProductTypeUnit.getText().toString().trim() + " )";
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("oldname", selectedProductType);
                            jsonObject.put("newname", updatedProductType);
                        } catch (JSONException e) {
                            Log.e("Edit prod type", "Json creation exception : " + e.getMessage());
                        }
                        new UpdateProductTypeTask(updatedProductType, position).execute(jsonObject);
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        }
        else
        {
            Toast.makeText(this,R.string.label_toast_Please_check_internet_connection,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProcessOrder(int orderId) {
        System.out.println("-----------------process order-----------------"+orderId);

        new ChangeSavedToProcessedOrder(this).execute(""+orderId);

    }

    @Override
    public void onClose() {
        System.out.println("----------------close order-----------------");
        dialogFragment.dismiss();

    }

    @Override
    public void onCancelDialog() {

    }

    //-----------------------Class for view bill for processed orders---------------------------------




    public class ViewBillAsyncTask extends AsyncTask<String,String,String>
    {
        Context context;
        ProgressDialog pd;

        ViewBillAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(DashboardActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String status;
            HttpURLConnection linkConnection = null;
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String emailid = sharedPreferences.getString("emailid","test2@gmail.com");
            String password =  sharedPreferences.getString("password","password");

            try {

                URL linkurl = new URL("http://ruralict.cse.iitb.ac.in/RuralIvrs/api/"+AdminDetails.getAbbr()+"/viewbill/"+params[0]);
                linkConnection = (HttpURLConnection) linkurl.openConnection();
                String basicAuth = "Basic " + new String(Base64.encode((emailid + ":" + password).getBytes(), Base64.NO_WRAP));
                linkConnection.setRequestProperty("Authorization", basicAuth);
                linkConnection.setDefaultUseCaches(false);
                linkConnection.setRequestMethod("GET");
                linkConnection.setRequestProperty("Accept", "text/html");
                linkConnection.setDoInput(true);
                InputStream is = null;

                status=String.valueOf(linkConnection.getResponseCode());


                if(status.equals("200"))
                {


                    is=linkConnection.getInputStream();
                }
                else
                { status="exception";
                    return  status;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();
                status = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
                status="exception";
                return status;
            }
            finally {
                if (linkConnection != null) {
                    linkConnection.disconnect();
                }
            }

            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.equals("exception"))
            {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Bill");
                WebView wv = new WebView(context);
                //  wv.loadUrl("http://www.google.com");
                wv.loadData(s, "text/html", "UTF-8");
                wv.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);

                        return true;
                    }
                });
                alert.setView(wv);
                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }


        }
    }





    //-----------------------------------------Class for change saved to processed orders-------------------------------


    public class ChangeSavedToProcessedOrder extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        Context context;
        String response;

        public ChangeSavedToProcessedOrder(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(DashboardActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String changeSavedToProcessedOrderURL = master.getChangeSavedToProcessedOrderURL() + Integer.valueOf(params[0]);
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(changeSavedToProcessedOrderURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            pd.dismiss();
            System.out.println(response);
            if (response.equals("exception"))
            {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try
                {
                    responseObject = new JSONObject(response);
                    if(responseObject.getString("result").equals("success"))
                    {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_Change_saved_to_processed_order, Toast.LENGTH_SHORT).show();
                        dialogFragment.dismiss();

                        fragmentClass = OrderFragment.class;
                        try {

                            fragment = (Fragment) fragmentClass.newInstance();


                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                        fm.beginTransaction().replace(R.id.content_frame, fragment).commit();

                    }

                    else

                        Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();

                }
                catch (Exception e)
                {
                    Log.e(LOG_TAG + " Parsing JSON error", e.getMessage());
                }
            }
        }
    }



    //----------------------------------------------------------------------------------------------------------------------

  /*  @Override
    public void onOrderItemSelected(int position) {

    }*/

    public class GetAdminAndProductTypeDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        Context context;
        String response;

        public GetAdminAndProductTypeDetails(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(DashboardActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String adminDetailsAndProductTypeURL = master.getAdminDetailsAndProductTypeURL() + "?phoneNumber=91" + AdminDetails.getMobileNumber();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(adminDetailsAndProductTypeURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            pd.dismiss();
            System.out.println(response);
            if (response.equals("exception"))
            {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try
                {
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray("productTypes");
                    for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                    {
                        productTypeArrayList.add(recyclerViewIndex, jsonArray.getString(recyclerViewIndex));
                    }
                    mAdapter.notifyDataSetChanged();
                }
                catch (Exception e)
                {
                    Log.e(LOG_TAG + " Parsing JSON error", e.getMessage());
                }
            }
        }
    }

    class GCMDeregisterTask extends AsyncTask<JSONObject, Void, Void> {

        Context context;
        GCMDeregisterTask(Context context)
        {
            this.context = context;
        }
        protected Void doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
            String response = getJson.getJSONFromUrl(Master.getDeregisterTokenURL(),params[0],"POST",false,null,null);
            Log.e(TAG,"Deregister response" + response);
            return null;
        }
    }
//-----------------------Class for updating a product type-------------------------------------
    class UpdateProductTypeTask extends AsyncTask<JSONObject, String, String>
    {
        String response, editProductTypeName;
        int editProductTypePosition;

        UpdateProductTypeTask(String editProductTypeName, int editProductTypePosition)
        {
            this.editProductTypeName = editProductTypeName;
            this.editProductTypePosition = editProductTypePosition;
        }

        @Override
        protected void onPreExecute() {
            Log.e("Vish", "in preexecute edit product type");
            Log.e("EP Name", editProductTypeName);
            Log.e("EP position", editProductTypePosition +"");
            progressDialog = new ProgressDialog(DashboardActivity.this);
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getEditProductTypeURL(), params[0],"POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message)
        {
            progressDialog.dismiss();
            Log.e("Edit ProdType response", message);
            try
            {
                responseObject = new JSONObject(message);
                message = responseObject.getString("edit");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            if(message.equals("exception")) {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else if(message.equals("success"))
            {
                Toast.makeText(DashboardActivity.this, R.string.label_toast_Product_type_updated_successfully, Toast.LENGTH_SHORT).show();
                productTypeArrayList.set(editProductTypePosition, editProductTypeName);
                System.out.println(productTypeArrayList);
                mAdapter.notifyItemChanged(editProductTypePosition);
            }
            else
            {
                Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }
        }
    }
//---------------------------------------------------------------------------------------------


//-----------------------Class for deleting a product type-------------------------------------
    /**************** API Format************************
    API:
    api/producttype/delete

    Body:
    {
        "orgabbr":"Test2",
            "name":"aba (kg)"
    }
    Response:
    {"message":"Product Type cannot be deleted as products of this type have been ordered"}
    OR
    {  "message":"Successfully deleted Product Type" }
    ***************************************************/
    class DeleteProductTypeTask extends AsyncTask<JSONObject, String, String>
    {
        String response;
        int productTypePosition;

        DeleteProductTypeTask(int productTypePosition)
        {
            this.productTypePosition = productTypePosition;
        }

        @Override
        protected void onPreExecute() {
            Log.e("Vish", "in preexecute delete product type");

            progressDialog = new ProgressDialog(DashboardActivity.this);
            progressDialog.setMessage(getString(R.string.label_please_wait));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteProductTypeURL(), params[0],"POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            progressDialog.dismiss();

            Log.e("Del ProdType response", message);
            try
            {
                responseObject = new JSONObject(message);
                message = responseObject.getString("message");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            if(!Master.isNetworkAvailable(DashboardActivity.this))
            {
                Log.e("Vish", "isnetavail");
                Toast.makeText(DashboardActivity.this, R.string.label_toast_Please_check_internet_connection + "", Toast.LENGTH_SHORT);
            }
            else if(message.equals("exception"))
            {
                Log.e("Vish", "exception");
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else if(message.equals("Successfully deleted Product Type"))
            {
                Log.e("Delete pro type", "successful");
                Toast.makeText(DashboardActivity.this, R.string.label_toast_product_type_deleted_successfully, Toast.LENGTH_SHORT).show();
                productTypeArrayList.remove(productTypePosition);
                System.out.println(productTypeArrayList);
                mAdapter.notifyItemRemoved(productTypePosition);
            }
            else
            {
                Log.e("Delete pro type", "unsuccessful");
                Toast.makeText(DashboardActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }
//---------------------------------------------------------------------------------------------


    //---------------------------Fragment classes--------------------------------------------------
    public static class ProductFragment extends Fragment
    {

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);
            getActivity().setTitle(R.string.title_product_types);

            return productFragmentView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);

            mRecyclerView = (RecyclerView) getView().findViewById(R.id.productTypeRecyclerView);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new RecyclerViewAdapter(productTypeArrayList, getActivity());
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView,1,null));
        }


    }
/*    public static class OrderFragment extends Fragment
    {
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_order, container, false);
            getActivity().setTitle(R.string.title_orders);
            return rootView;
        }
    }

    public static class MemberFragment extends Fragment
    {
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_member, container, false);
            getActivity().setTitle(R.string.title_members);
            return rootView;
        }
    }

    public static class SettingsFragment extends Fragment
    {
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
            getActivity().setTitle(R.string.title_settings);
            return rootView;
        }
    }
*/
//-------------------------------End of fragment classes------------------------------------------
}


